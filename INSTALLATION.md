# Installation

## dependencies
This application uses poetry to setup a virtual environment with the required python
dependencies. YOu can install thoe dependencies yourself using `pip`, in an interpreter 
of your choosing, but it's probably more cumbersome. If you want to know what you need:
check the `pyproject.toml` file.

Using poetry would require you to have poetry>0.12 and python>3.8 installed and then
run:
```shell script
poetry install
```

# Running
## using poetry
preferably let poetry manage your virtual python environment end therefore use poetry
to start the application
run the shell script
```shell script
poetry run fomse.py
```

or the package
```shell script
poetry run python -m free_open_music_score_editor
```

## without poetry
run the shell script
```shell script
fomse.py
```

or the package
```shell script
python -m free_open_music_score_editor
```

For supported command line options, supply the commandline option `--help`
