# free_open_musice_score_editor

The Free Open Music Score Editor is an application that lets the user create and view
music scores. It aims to be cross platform, but is only tested under Linux. If it needs
work to have it running under any other operating system, feel free to 
[contribute](CONTRIBUTING.md).
