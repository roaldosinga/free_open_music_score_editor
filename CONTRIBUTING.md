# Contributing

## Prepartion
When setting up the project in PyCharm, I've have problems with pycharm recognising
my virtual environment with the default _poetry_ configuration. Configuring _poetry_
to place the virtual environment inside the project folder solved the problem.

Configure poetry for it like this:
```shell script
poetry config virtualenvs.in-project true
```

Install the required dpendencies with:
```shell script
poetry install
```

Or update the the latest version of packages that _should be_ compatible using`
```shell script
poetry update
```

After installation of dependencies, please run the automated tests.
